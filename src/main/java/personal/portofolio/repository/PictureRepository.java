package personal.portofolio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import personal.portofolio.model.Picture;

@Repository
public interface PictureRepository extends JpaRepository<Picture, Integer> {

}
