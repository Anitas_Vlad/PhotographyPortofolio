package personal.portofolio.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PictureDto {

    private String image;
}
