package personal.portofolio.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;


@Component
public class PictureValidator {

    public void validate(MultipartFile multipartFile, BindingResult bindingResult) {
        try {
            byte[] image = multipartFile.getBytes();
            if (image.length > 0) {
                return;
            }
        } catch (Exception e) {
        }
        FieldError fieldError = new FieldError("pictureDto", "image", "Invalid image");
        bindingResult.addError(fieldError);
    }

    public void validatePictures(ArrayList<MultipartFile> multipartFiles, BindingResult bindingResult) {
        multipartFiles.forEach(multipartFile -> {
            try {
                byte[] image = multipartFile.getBytes();
                if (image.length > 0) {
                    return;
                }
            } catch (Exception e) {
            }
        });
        FieldError fieldError = new FieldError("pictureDto", "image", "Invalid image");
        bindingResult.addError(fieldError);
    }
}
