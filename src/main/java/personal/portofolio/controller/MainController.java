package personal.portofolio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import personal.portofolio.dto.PictureDto;
import personal.portofolio.service.PictureService;
import personal.portofolio.validator.PictureValidator;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private PictureService pictureService;

    @Autowired
    private PictureValidator pictureValidator;

    @GetMapping(value = "/home")
    public String homeGet() {
        return "home";
    }

    @GetMapping(value = "/addPicture")
    public String addPictureGet(Model model, @ModelAttribute("addPictureMessage") String addPictureMessage) {
        PictureDto pictureDto = new PictureDto();
        model.addAttribute("pictureDto", pictureDto);
        return "addPicture";
    }

    @PostMapping(value = "/addPicture")
    public String addPicturePost(@ModelAttribute(name = "pictureDto") PictureDto pictureDto, BindingResult bindingResult,
                                 @RequestParam(value = "pictureImage", required = false) MultipartFile multipartFile,
                                 RedirectAttributes redirectAttributes) {
        pictureValidator.validate(multipartFile, bindingResult);
        if (bindingResult.hasErrors()){
            return "/addPicture";
        }
        pictureService.addPicture(multipartFile);
        redirectAttributes.addFlashAttribute("addPictureMessage", "The image was successfully added.");
        return "redirect:/addPicture";
    }

    @GetMapping(value = "/addPictures")
    public String addPicturesGet(Model model, @ModelAttribute("addPicturesMessage") String addPicturesMessage) {
        List<PictureDto> pictureDtoList = new ArrayList<>();
        model.addAttribute("pictureDtoList", pictureDtoList);
        return "addPictures";
    }

    @PostMapping(value = "/addPictures")
    public String addPicturesPost(@ModelAttribute(name = "pictureDto") PictureDto pictureDto, BindingResult bindingResult,
                                  @RequestParam(value = "pictureImage", required = false) MultipartFile multipartFile,
                                  RedirectAttributes redirectAttributes) {

        return "redirect:/addPictures";
    }

    @GetMapping(value = "/gallery")
    public String galleryGet(Model model) {
        List<PictureDto> pictureDtoList = pictureService.viewAllPictures();
        model.addAttribute(pictureDtoList);
        return "gallery";
    }
}
