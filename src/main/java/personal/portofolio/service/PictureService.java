package personal.portofolio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import personal.portofolio.dto.PictureDto;
import personal.portofolio.mapper.PictureMapper;
import personal.portofolio.model.Picture;
import personal.portofolio.repository.PictureRepository;

import java.util.ArrayList;
import java.util.List;


@Service
public class PictureService {

    @Autowired
    private PictureRepository pictureRepository;

    @Autowired
    private PictureMapper pictureMapper;

    public void addPicture(MultipartFile multipartFile){
        Picture picture = pictureMapper.map(multipartFile);
        pictureRepository.save(picture);
    }

    public void addPictures(ArrayList<MultipartFile> multipartFiles){
        multipartFiles.forEach(multipartFile -> {
            Picture picture = pictureMapper.map(multipartFile);
            pictureRepository.save(picture);
        });
    }

    public List<PictureDto> viewAllPictures(){
        List<Picture> pictures = pictureRepository.findAll();
        List<PictureDto> result = new ArrayList<>();
        pictures.forEach(picture -> {
            result.add(pictureMapper.map(picture));
        });
        return result;
    }
}
