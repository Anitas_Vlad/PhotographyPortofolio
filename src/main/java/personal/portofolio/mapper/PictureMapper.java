package personal.portofolio.mapper;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import personal.portofolio.dto.PictureDto;
import personal.portofolio.model.Picture;
import org.apache.tomcat.util.codec.binary.Base64;

import java.io.IOException;

@Component
public class PictureMapper {

    public Picture map( MultipartFile multipartFile){
        Picture picture = new Picture();
        picture.setImage(getBytes(multipartFile));
        return picture;
    }

    private byte[] getBytes(MultipartFile multipartFile){
        try{
            return multipartFile.getBytes();
        } catch (IOException e) {
            return new byte[0];
        }
    }

    public PictureDto map(Picture picture){
        PictureDto pictureDto = new PictureDto();
        pictureDto.setImage(Base64.encodeBase64String(picture.getImage()));
        return pictureDto;
    }
}
